package com.yogi.common.lib;

import com.yogi.common.selenium.Driver;
import org.openqa.selenium.By;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public abstract class Topic {

    public abstract void getTopic();

    public abstract void goToSubTopic(String topicName);

    public void clickApply(){
        Driver.getDriver().findElement(By.id("doaction")).click();
    };

    public static String msgOnPage(By elementType){

      String myMsgInWordpress =  Driver.getDriver().findElement(elementType).getText();
       return myMsgInWordpress ;
    }

    public static void findElement(By elementType){
        Driver.getDriver().findElement(elementType).click();
    }

}
