package com.yogi.common.lib;

import com.yogi.pageobjects.PageOfWordPress;
import com.yogi.pageobjects.PostPage;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public class TopicFactory {

    public static Topic getTopicType(String topicType){
        Topic topic=null;

        switch (topicType.toUpperCase()){
            case "POST":
                topic=new PostPage();
                break;
            case "PAGE":
                topic=new PageOfWordPress();
                break;
                default:
                    throw new IllegalArgumentException("Invalid Topic");
        }
        return topic;
    }
}
