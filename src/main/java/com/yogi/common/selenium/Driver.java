package com.yogi.common.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public class Driver {
    private static WebDriver driver;
    private static String path = "application.properties";

    private Driver(){
    }

   public  String getEnv(String url) {
        Properties prop = new Properties();
        InputStream input=null;
        input = getClass().getClassLoader().getResourceAsStream(path);
       try {
           prop.load(input);
       } catch (IOException e) {
           e.printStackTrace();
       }
       return prop.getProperty(url);

    }

    public static WebDriver getDriver(){
     if(driver==null) {
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
            driver = new ChromeDriver();
            driver.get(new Driver().getEnv("baseURLQA"));
            System.out.println("my env is " + new Driver().getEnv("baseURLQA"));
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        }
        return driver;
    }


    public static void cleanUp(){
        driver.quit();
        driver.close();
    }


}
