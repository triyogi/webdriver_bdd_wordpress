package com.yogi.pageobjects;

import com.yogi.common.selenium.Driver;
import com.yogi.common.lib.Topic;
import org.openqa.selenium.By;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public class PageOfWordPress extends Topic {

    @Override
    public void getTopic() {
        Driver.getDriver().findElement(By.xpath("//*[@id=\"menu-pages\"]/a/div[3]")).click();
    }

    public void goToSubTopic(String subTopic){
        switch (subTopic.toUpperCase()) {
            case "ALL PAGES":
                Driver.getDriver().findElement(By.xpath("//*[@id=\"menu-pages\"]/ul/li[2]/a")).click();
                break;
            case "ADD NEW":
                Driver.getDriver().findElement(By.xpath("//*[@id=\"menu-pages\"]/ul/li[3]/a")).click();
                break;
                default:
                    throw new IllegalArgumentException("Invalid Sub Topic");
        }
    }
}
