package com.yogi.pageobjects;

import com.yogi.pageobjectcommands.Login.LoginCommand;
import com.yogi.pageobjectcommands.Login.LostCredientialsCommand;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public class LoginPage {

    public static LoginCommand LoginAs(){

        return new LoginCommand();
    }

    public static LostCredientialsCommand resettingPassword(){

        return new LostCredientialsCommand();
    }
}
