package com.yogi.pageobjectcommands.Login;

import com.yogi.common.selenium.Driver;
import org.openqa.selenium.By;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public class LostCredientialsCommand {

    private String emailId;

    public LostCredientialsCommand withEmailId(String emailId){
        this.emailId = emailId;
        return this;
    }

    public void doResetPassword(){

        Driver.getDriver().findElement(By.linkText("Lost your password?")).click();
        Driver.getDriver().findElement(By.id("user_login")).sendKeys(this.emailId);
        Driver.getDriver().findElement(By.id("wp-submit")).click();
    }

}
