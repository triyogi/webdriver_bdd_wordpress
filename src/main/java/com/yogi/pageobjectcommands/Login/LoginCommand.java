package com.yogi.pageobjectcommands.Login;

import com.yogi.common.selenium.Driver;
import org.openqa.selenium.By;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public class LoginCommand {

    private String userName;
    private String password;

    public LoginCommand withUserName(String userName){
        this.userName=userName;
        return this;
    }

    public LoginCommand withPassword(String password){
        this.password=password;
        return this;
    }

    public LoginCommand rememberMe(){
        Driver.getDriver().findElement(By.id("rememberme")).click();
        return this;
    }

    public void lostPassword(){
        Driver.getDriver().findElement(By.linkText("Lost your password?")).click();
    }

    public void doLogin(){
        Driver.getDriver().findElement(By.id("user_login")).sendKeys(userName);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Driver.getDriver().findElement(By.id("user_pass")).sendKeys(password);
        Driver.getDriver().findElement(By.id("wp-submit")).click();
    }
}
