package com.yogi.pageobjectcommands.DashBoard;
import com.yogi.common.selenium.Driver;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public class DashboardCommand {

    private String newPostName;
    private String postDescription;



    public DashboardCommand setPostDetails(List<String> postDetails){
        this.newPostName = postDetails.get(0);
        this.postDescription = postDetails.get(1);
        return this;
    }

    public DashboardCommand selectPost(){
        Driver.getDriver().findElement(By.linkText("Posts")).click();
        return this;
    }

    public void createNewPost(){
        Driver.getDriver().findElement(By.linkText("Add New")).click();
        Driver.getDriver().findElement(By.id("title")).sendKeys(newPostName);
        Driver.getDriver().findElement(By.id("content_ifr")).sendKeys(postDescription);
        Driver.getDriver().findElement(By.id("publish")).click();
    }


}
