package com.yogi.stepdef;

import com.yogi.common.lib.Topic;
import com.yogi.common.selenium.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public class BaseStepDef {

   String path="application.properties";

    public String getFindElement(String itemType){

        String myMgs=null;

        Properties prop = new Properties();
        InputStream input= null;
        input = getClass().getClassLoader().getResourceAsStream(path);
        try {
            prop.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
       return Topic.msgOnPage(By.xpath(prop.getProperty(itemType.toUpperCase())));
    }

    public void logOut(){
        Actions actions = new Actions(Driver.getDriver());
        WebElement menu = Driver.getDriver().findElement(By.id("wp-admin-bar-my-account"));
        actions.moveToElement(menu).perform();
        Driver.getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement subMenu = Driver.getDriver().findElement(By.xpath("/*//*[@id=\"wp-admin-bar-logout\"]/a"));
        actions.moveToElement(subMenu).perform();
       // actions.click().build().perform();
        subMenu.click();

    }

}
