package com.yogi.stepdef;
import com.yogi.common.selenium.Driver;
import com.yogi.pageobjects.LoginPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public class LoginStepdef extends BaseStepDef{

    @Given("^I am on word press app login page$")
    public void i_am_on_word_press_app_login_page() throws Throwable {
        Driver.getDriver();
    }

    @When("^I login with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_login_with_and(String userName, String password) throws Throwable {

        Thread.sleep(1000);

        Driver.getDriver().get("http://localhost:20165/wp-login.php");
        LoginPage.LoginAs().withUserName(userName).withPassword(password).doLogin();
    }

    @When("^I login with \"([^\"]*)\" and \"([^\"]*)\" with remember me checked$")
    public void i_login_with_and_with_remember_me_checked(String userName, String password) throws Throwable {
        LoginPage.LoginAs().withUserName(userName).withPassword(password).rememberMe().doLogin();
    }

    @Then("^I should navigate to home page$")
    public void i_should_navigate_to_home_page() throws Throwable {
    }

    @When("^I click on lost password link on home page with email id \"([^\"]*)\"$")
    public void i_click_on_lost_password_link_on_home_page(String emailId) throws Throwable {
       // LoginPage.LoginAs().lostPassword();
        LoginPage.resettingPassword().withEmailId(emailId).doResetPassword();
    }

    @Then("^I should navigate to password recovery page$")
    public void i_should_navigate_to_password_recovery_page() throws Throwable {
    }

    @When("^I create a new post \"([^\"]*)\" with \"([^\"]*)\"$")
    public void i_create_a_new_post_with(String newPostName,String postDescription) throws Throwable {
      //  DashboardPage.CreatePostAs().selectPost().setPostTitle(newPostName).setPostDescription(postDescription).createNewPost().publishPost();
    }

    @Then("^I post should be created$")
    public void i_post_should_be_created() throws Throwable {
    }

    @Then("^I logout$")
    public void i_logout() throws Throwable {
        logOut();


        //Driver.getDriver().findElement(By.id("//*[@id=\"wp-admin-bar-logout\"]")).click();


    /*    Actions builder = new Actions(Driver.getDriver());
        builder.moveToElement(Driver.getDriver().findElement(By.id("wp-admin-bar-my-account"))).moveToElement(Driver.getDriver().findElement(By.xpath("/*//*[@id=\"wp-admin-bar-logout\"]/a"))).click().perform();
*/

      /*  Actions actions = new Actions(Driver.getDriver());
        WebElement menu = Driver.getDriver().findElement(By.id("wp-admin-bar-my-account"));
        actions.moveToElement(menu);

        WebElement subMenu = Driver.getDriver().findElement(By.xpath("/*//*[@id=\"wp-admin-bar-logout\"]/a"));
        actions.moveToElement(subMenu);
        actions.click().build().perform();*/

    }
}
