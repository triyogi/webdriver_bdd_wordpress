package com.yogi.stepdef;

import com.yogi.common.lib.Topic;
import com.yogi.common.lib.TopicFactory;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public class TopicStepDef extends  BaseStepDef{

    @When("^I go to \"([^\"]*)\" of \"([^\"]*)\" on Dashboard$")
    public void i_go_to_of_on_Dashboard(String subTopic, String topicName) throws Throwable {
          Topic topic = TopicFactory.getTopicType(topicName);

        topic.getTopic();
        topic.goToSubTopic(subTopic);

       // topic.clickApply();
    }

    @Given("^I get the \"([^\"]*)\" message on page$")
    public void i_get_the_message_on_page(String itemType) throws Throwable {
        String getMessageText=getFindElement(itemType);
        System.out.println(getMessageText);
    }

}
