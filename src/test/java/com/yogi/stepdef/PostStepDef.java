package com.yogi.stepdef;

import com.yogi.pageobjects.DashboardPage;
import cucumber.api.java.en.When;

import java.util.List;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
public class PostStepDef {


    @When("^I create a new post with following title and description$")
    public void i_create_a_new_post_with_following_title_and_description(List<String> postDetails) throws Throwable {


     // DashboardPage.CreatePostAs().selectPost().setPostTitle(postDetails.get(0)).setPostDescription(postDetails.get(1)).createNewPost().publishPost();

        DashboardPage.CreatePostAs().selectPost().setPostDetails(postDetails).createNewPost();

        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to 4one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
    }
}
