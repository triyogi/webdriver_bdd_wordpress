package com.yogi.testrunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Yogita Tripathi on 09/09/2017.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:cucumber/"}
        ,glue = {"classpath:"}
      //,dryRun =true
       // ,tags = {"@reusedemo"}
)
public class CucumberRunner {
}
