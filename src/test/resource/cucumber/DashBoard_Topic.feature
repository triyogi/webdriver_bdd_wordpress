Feature: Verify DashBoard operations on WordPress

  Background:
    Given I login with "admin" and "Jantar786@Mantar"

  @factory
  Scenario: Go to All Post on PAGE using factory
  When I go to "All Posts" of "Post" on Dashboard
  Then I post should be created
  And I logout

  @factory
  Scenario: Go to All Post on POST page using factory
  When I go to "All Pages" of "Page" on Dashboard
  Then I post should be created
  And I logout

  @reusedemo
  Scenario: Read any string lable on Page or Post of DashBoard page
   When I go to "All Posts" of "Post" on Dashboard
    * I get the "allcount" message on page
    * I get the "postcount" message on page
    And I logout