Framework demonstrate following features:
1. Singleton pattern to manage single webdriver instance and global waut properties(Fluent/implicit)
2. Maven using maven-compiler plugin for1.8 and cuke/web/guava/common dependencies
3. Cucumber using cucumber junit & cucumber options e.g. glue,-dryrun and tags.
4. Factory pattern to increase reusability of common components (e.g. Topic in Page and Post)
5. Builder pattern to manage navigation and flexibility to build steps for tester using "." operator
6. Tags to run multiple Scenarios
7. Property file for reusable element,env,url,database property
8. Combine all library in page object model and behavior in command builders
9. Video to depict the framework hosted @ https://yogita245-gmail.tinytake.com/sf/MTk0MDczOF82MTY3ODQ1


